<?php


namespace Safebits\Configuration\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Safebits\Configuration\Models\SCModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SCModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SCModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SCModel query()
 * @mixin \Eloquent
 */
class SCModel extends Model
{
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $connection;

    /**
     * SCModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config('safebits_system_configuration.connection', 'mysql');
    }
}
