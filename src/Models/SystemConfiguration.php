<?php

namespace Safebits\Configuration\Models;

use Illuminate\Support\Facades\Crypt;
use Safebits\Configuration\Helpers\VariablesHelper;

/**
 * Safebits\Configuration\Models\SystemConfiguration
 *
 * @property int $systemConfigurationId
 * @property string $tag
 * @property mixed $value
 * @property string $type
 * @property string $description
 * * @property boolean $encrypted
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereSystemConfigurationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Safebits\Configuration\Models\SystemConfiguration whereEncrypted($value)
 * @mixin \Eloquent
 */
class SystemConfiguration extends SCModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'systemConfigurationId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * System constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('system_configuration');
    }

    /**
     * Binds function to system configuration create and save events.
     */
    public static function boot()
    {
        parent::boot();

        static::saving(function (SystemConfiguration $systemConfiguration) {
            if ($systemConfiguration->encrypted && !$systemConfiguration->getOriginal('encrypted')) {
                $systemConfiguration->value = Crypt::encrypt($systemConfiguration->value);
            } elseif (!$systemConfiguration->encrypted && $systemConfiguration->getOriginal('encrypted')) {
                $systemConfiguration->value = Crypt::decrypt($systemConfiguration->value);
            } elseif ($systemConfiguration->encrypted && $systemConfiguration->getOriginal('encrypted')) {
                $originalValue = Crypt::decrypt($systemConfiguration->getOriginal('value'));
                $newValue = Crypt::decrypt($systemConfiguration->value);
                if ($originalValue != $newValue) {
                    $systemConfiguration->value = Crypt::encrypt($newValue);
                }
            }
        });
    }

    /**
     * @param $configurationTag
     * @param null $defaultValue
     * @return array|mixed|null
     * @throws \Safebits\Configuration\Exceptions\InvalidDataTypeException
     */
    public static function getConfigurationByTag($configurationTag, $defaultValue = null)
    {
        $configurationValue = env($configurationTag);

        if (!$configurationValue) {
            $foundConfiguration = self::where('tag', $configurationTag)->first();

            if ($foundConfiguration) {
                $decryptedConfigurationValue = $foundConfiguration->encrypted ? Crypt::decrypt($foundConfiguration->value) : $foundConfiguration->value;
                $configurationValue = VariablesHelper::castValueType($decryptedConfigurationValue, $foundConfiguration->type);
            } else {
                $configurationValue = $defaultValue;
            }
        }

        return $configurationValue;
    }
}
