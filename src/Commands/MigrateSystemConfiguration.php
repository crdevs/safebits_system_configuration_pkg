<?php

namespace Safebits\Configuration\Commands;

use Illuminate\Console\Command;

/**
 * Class MigrateSystemConfiguration
 * @package Safebits\Configuration\Commands
 */
class MigrateSystemConfiguration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sb:migrate-system-configuration {--refresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs Safebits Configuration migrations ';

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        //Laravel does not allow full paths when executing migrations
        $fullPath = dirname(__DIR__) . "/Database/Migrations";
        $migrationsPath = str_replace(base_path(), '', $fullPath);

        //Checks if refresh was requested
        if ($this->hasOption('refresh') && $this->option('refresh')) {
            $migrationCommand = 'migrate:refresh';
        } else {
            $migrationCommand = 'migrate';
        }

        //Connection is required in order to create migrations table
        $connection = config('safebits_system_configuration.connection');

        \Schema::connection($connection)->disableForeignKeyConstraints();

        //Executes migrations
        \Artisan::call($migrationCommand, array('--path' => $migrationsPath, '--force' => true, '--database' => $connection));

        \Schema::connection($connection)->enableForeignKeyConstraints();
    }
}
