<?php


namespace Safebits\Configuration\Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class SCSeeder
 * @package Safebits\Configuration\Database\Seeders
 */
class SCSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * CommonSeeder constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_system_configuration.connection', 'mysql');
    }

}
