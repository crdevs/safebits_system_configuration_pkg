<?php


namespace Safebits\Configuration\Database\Migrations;

use Illuminate\Database\Migrations\Migration;

/**
 * Class SCMigration
 * @package Safebits\Configuration\Migrations
 */
class SCMigration extends Migration
{
    /**
     * @var string
     */
    protected $connection;

    /**
     * SCMigration constructor.
     */
    public function __construct()
    {
        $this->connection = config('safebits_system_configuration.connection');
    }
}
