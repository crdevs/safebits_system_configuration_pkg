<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateSystemConfigurationTable
 */
class CreateSystemConfigurationTable extends \Safebits\Configuration\Database\Migrations\SCMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_configuration', function (Blueprint $table) {
            $table->increments('systemConfigurationId');
            $table->string('tag', 100);
            $table->binary('value');
            $table->string('type');
            $table->string('description', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('system_configuration');
    }
}
