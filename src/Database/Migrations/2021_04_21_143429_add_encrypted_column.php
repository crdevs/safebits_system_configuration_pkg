<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddEncryptedColumn
 */
class AddEncryptedColumn extends \Safebits\Configuration\Database\Migrations\SCMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_configuration', function (Blueprint $table) {
            $table->boolean('encrypted')->after('type')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_configuration', function (Blueprint $table) {
            $table->dropColumn('encrypted');
        });
    }
}
