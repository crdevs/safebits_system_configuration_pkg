<?php

namespace Safebits\Configuration;

use Illuminate\Support\ServiceProvider;
use Safebits\Configuration\Commands\MigrateSystemConfiguration;

/**
 * Class SystemConfigurationServiceProvider
 * @package Safebits\Configuration
 */
class SystemConfigurationServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    // Registers provider system configuration
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateSystemConfiguration::class,
            ]);
        }

        // Publishes config file to local configuration path
        $this->publishes([
            __DIR__ . '/Config/safebits_system_configuration.php' => config_path('safebits_system_configuration.php'),
        ]);
    }
}
