<?php

namespace Safebits\Configuration\Exceptions;

/**
 * Class InvalidDataTypeException
 * @package Safebits\Configuration\Exceptions
 */
class InvalidDataTypeException extends ConfigurationException
{
    /**
     * InvalidDataTypeException constructor.
     * @param $message
     */
    public function __construct($message = null)
    {
        $message = $message ? $message : 'The given data type is not valid';
        parent::__construct(500, $message);
    }
}
