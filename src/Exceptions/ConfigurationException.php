<?php

namespace Safebits\Configuration\Exceptions;

/**
 * Class ConfigurationException
 * @package Safebits\Configuration\Exceptions
 */
class ConfigurationException extends \Exception
{
    /**
     * ConfigurationException constructor.
     * @param null $statusCode
     * @param null $message
     * @param null $previous
     */
    public function __construct($statusCode = null, $message = null, $previous = null)
    {
        if ($statusCode == null) {
            $statusCode = 500;
        }

        parent::__construct($message, $statusCode, $previous);
    }
}
