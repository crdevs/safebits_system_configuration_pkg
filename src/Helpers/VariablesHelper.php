<?php

namespace Safebits\Configuration\Helpers;

use Safebits\Configuration\Exceptions\InvalidDataTypeException;

/**
 * Class VariablesHelper
 * @package Safebits\Configuration\Helpers
 */
class VariablesHelper
{
    /**
     * Array data type
     */
    const ARRAY_TYPE = 'array';

    /**
     * Json data type
     */
    const JSON_TYPE = 'json';

    /**
     * Boolean data type
     */
    const BOOLEAN_TYPE = 'boolean';

    /**
     * Integer data type
     */
    const INTEGER_TYPE = 'integer';

    /**
     * Float data type
     */
    const FLOAT_TYPE = 'float';

    /**
     * String data type
     */
    const STRING_TYPE = 'string';

    /**
     * Double data type
     */
    const DOUBLE_TYPE = 'double';

    /**
     * Variable types that can be casted using PHP methods
     */
    const PHP_SUPPORTED_VARIABLE_TYPES = [self::BOOLEAN_TYPE, self::INTEGER_TYPE, self::FLOAT_TYPE, self::DOUBLE_TYPE, self::STRING_TYPE];

    /**
     * @param $value
     * @param $type
     * @return array|mixed
     * @throws InvalidDataTypeException
     */
    public static function castValueType($value, $type)
    {
        $type = strtolower($type);

        switch ($type) {
            case in_array($type, self::PHP_SUPPORTED_VARIABLE_TYPES):
                settype($value, $type);
                break;
            case self::ARRAY_TYPE:
                $value = explode(',', $value);
                break;
            case self::JSON_TYPE:
                $value = json_decode($value);
                break;
            default:
                throw new InvalidDataTypeException();
                break;
        }

        return $value;
    }
}
